# Trezò-Bash-Scripts

This is just a collection of bash scripts I've written to do various things. Some are simply launchers for things I'm to lazy to alias, some do some cool useful things though.

Trezò is Haitian Creole, for treasure. I added that to the repo name, because "Bash-Scripts" sounded too vague.
